<?php

require_once(drupal_get_path('module', 'lm_paypal') . '/tests/abstract.test');

class LmPaypalDonationsTestCase extends AbstractLmPaypalTestCase {

  static function getInfo() {
    return array(
      'name' => t('Donations'),
      'description' => t('Tests the creation and processing of donations.'),
      'group' => t('LM Paypal'),
    );
  }

  function setUp() {

    // Always call the setUp() function from the parent class.
    parent::setUp('lm_paypal_donations', 'block', 'php');

    // Create users.
    $admin_permissions = array(
      'administer lm_paypal',
      'access lm_paypal_donate',
      'view lm_paypal_all_donations',
      'access administration pages',
      'administer blocks',
      'administer filters',
      'access user profiles',
    );
    $this->admin_user = $this->drupalCreateUser($admin_permissions);
    $this->web_user = $this->drupalCreateUser(array('access lm_paypal_donate'));

    // The return page for the PayPal form must be set.
    variable_set('lm_paypal_donations_thanks', LM_PAYPAL_DONATIONS_THANKS_DEFAULT);
  }

  function tearDown() {

    // Always call the tearDown() function from the parent class.
    parent::tearDown();
  }

  /**
   * Exercise the basic LM PayPal Donations pages.
   */
  function testLmPaypal() {

    // Check Settings page.
    $thanks_page = variable_get('lm_paypal_donations_thanks', NULL);
    $this->drupalLogin($this->admin_user);
    $this->drupalGet('admin/config/lm_paypal/donations_settings');
    $this->assertField('lm_paypal_donations_thanks');
    $this->assertRaw($thanks_page);

    // Check the help page.
    $this->clickLink('LM PayPal Donations Help');

    // Check the page configured as the thanks page.
    $this->drupalGet($thanks_page);

    // Check the (empty) Donations page
    $this->drupalGet('lm_paypal/donations');
    $this->assertText('No donations found.');

    // Create a custom block with three PayPal donation buttons in it.
    $this->_createPayPalBlock();
  }

  /**
   * Test IPN processing by submitting fake Donation IPNs.
   */
  function testDonationIPN() {

    // Check All Donations page - it should be empty.
    $this->drupalLogin($this->admin_user);
    $this->drupalGet('lm_paypal/donations');
    $this->assertText('No donations found.');

    // Post fake Donation IPNs (txn_id=0)
    $num_users = 3;
    $num_donations_per_user = 3;
    $ipns = array();
    for ($user = 0; $user < $num_users; $user++) {

      // Create a new user.
      $temp_user = $this->drupalCreateUser(array('access lm_paypal_donate'));
      $temp_user_name = $temp_user->name;

      // Make some donations, checking each as we go (using the admin user).
      $this->drupalLogin($this->admin_user);
      for ($donation = 0; $donation < $num_donations_per_user; $donation++) {

        // Post the IPN with some unique values.
        $offset = 100 * $user + 10 * $donation; // just a number to make values unique(ish)
        $ipn = $this->postPreparedIpn(
          'ipn-web_accept-inprogress.txt',
          array(
            'item_number' => 0,
            'item_name' => $this->randomName(6, 'ITEM_'),
            'receiver_email' => variable_get('lm_paypal_business', NULL),
            'payer_email' => $temp_user->mail,
            'mc_gross' => 52.34 + $offset,
            'mc_gross1' => 49.34 + $offset,
            'txn_id' => 548954 + $offset,
            'mc_currency' => 'NZD',
          )
        );

        // Check for errors processing IPN.
        $this->assertWatchdogError('IPN web_accept no uid, found payer_email%%');
        $this->assertNoWatchdogError('No web_accept processor registered for this item_number:%%');
        $this->assertNoWatchdogErrors(WATCHDOG_WARNING);

        // Check All Donations page for the new IPN.
        $this->drupalGet('lm_paypal/donations');
        $this->assertNoText('No donations found.');
        $this->assertLink($temp_user_name, 0, "Found payer name $temp_user_name");
        $this->assertText($ipn['item_name'], "Found item_name $ipn[item_name]");
        $this->assertText($ipn['mc_gross'], "Found item_name $ipn[mc_gross]");
        $this->assertText($ipn['mc_currency'], "Found item_name $ipn[mc_currency]");

         // Check user account page for the new IPN.
        $this->clickLink($temp_user_name);
        $this->assertNoText('No donations found.');
        $this->assertLink($temp_user_name, 0, "Found payer name $temp_user_name");
        $this->assertText($ipn['item_name'], "Found $ipn[item_name]");
        $this->assertText($ipn['mc_gross'], "Found $ipn[mc_gross]");
        $this->assertText($ipn['mc_currency'], "Found $ipn[mc_currency]");

        // Save the IPN.
        $ipns[$user][$donation] = $ipn;
      }

      // Check the user has all donations on their page (as admin).
      $this->drupalGet("user/$temp_user->uid");
      $this->assertNoText('No donations found.');
      foreach ($ipns[$user] as $ipn) {
        $this->assertText($ipn['item_name'], "Found $ipn[item_name]");
        $this->assertText($ipn['mc_gross'], "Found $ipn[mc_gross]");
        $this->assertText($ipn['mc_currency'], "Found $ipn[mc_currency]");
      }

      // Check the user has all donations on their page (as user).
      $this->drupalLogin($temp_user);
      $this->clickLink('My account');
      $this->assertText('Paypal Donations');
      $this->assertNoText('No donations found.');
      foreach ($ipns[$user] as $ipn) {
        $this->assertText($ipn['item_name'], "Found $ipn[item_name]");
        $this->assertText($ipn['mc_gross'], "Found $ipn[mc_gross]");
        $this->assertText($ipn['mc_currency'], "Found $ipn[mc_currency]");
      }
      $num_matches = preg_match_all('#NZD#is', $this->drupalGetContent(), $matches);
      $this->assertEqual($num_matches, $num_donations_per_user, "Found $num_matches NZD matches");
    }

    // Check All Donations page for all payments.
    $this->drupalLogin($this->admin_user);
    $this->drupalGet('lm_paypal/donations');
    $this->assertNoText('No donations found.');
    $num_matches = preg_match_all('#NZD#is', $this->drupalGetContent(), $matches);
    $this->assertEqual($num_matches, $num_users * $num_donations_per_user, "Found $num_matches NZD matches");

    // @todo Check for inprogress/completed.
  }


  function _createPayPalBlock() {

    // Enable PHP Input Format for authenticated users
    $edit = array(

      // Authenticated user
      'roles[2]' => TRUE,
    );
    $this->drupalPost('admin/config/content/formats/php_code', $edit, 'Save configuration');

    // @todo Create a Block with the Donate PHP in it.

    // Confirm that the block has been created, and then query the created ID.
    $this->assertText(t('The block has been created.'), t('block successfully created.'));
    $delta = db_query("SELECT delta FROM {block} WHERE title = :title", array(':title' => $edit['title']))
      ->fetchField();

    // Check to see if the block was created by checking that it's in the database.
    $this->assertNotNull($delta, t('block found in database'));

    // Set the created box to a specific region.
    // @todo Implement full region checking.
    $edit = array();
    $edit['blocks[block_' . $delta . '][region]'] = 'sidebar_first';
    $this->drupalPost('admin/structure/block', $edit, t('Save blocks'));

    // Check the block appeared and was executed correctly
    $this->drupalGet('/');
    $this->assertNoRaw('<?php', 'No unparsed PHP code found');
    $this->assertRaw('<form action="https://www.sandbox.paypal.com/cgi-bin/webscr"', 'Sandbox form(s) found');

    $this->assertText('We would really like a 10000 yen donation');
    $this->assertText('Proceed with your &#165; 10000 payment');
    $this->assertText('Write your own amount to give, we suggest 5 Euros');

    // @todo: Check textedit with default value of 5
    $this->assertText('Value is Euro');
    $this->assertText('We would really like a $10, or more, donation');

    // Check select list with suitable values! Default 10.
    $this->assertText('Value is Australian Dollar');
  }
}
