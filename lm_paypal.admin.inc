<?php

/**
 * @file
 * Administrative interface for the LM PayPal module.
 */

/**
 * Form constructor for a form to to set LM PayPal settings.
 *
 * Path: admin/config/lm_paypal/settings
 *
 * @todo: Add better support for switching between live and sandbox sites.
 */
function lm_paypal_settings_form($form, &$form_state) {
  $form['lm_paypal_business'] = array(
    '#type' => 'textfield',
    '#title' => t('PayPal email address'),
    '#default_value' => variable_get('lm_paypal_business', ''),
    '#maxlength' => 100,
    '#required' => TRUE,
    '#description' => t('The email address to receive payment.'),
  );
  $form['lm_paypal_host'] = array(
    '#type' => 'textfield',
    '#title' => t('LM PayPal Host'),
    '#default_value' => variable_get('lm_paypal_host', LM_PAYPAL_HOST_DEFAULT),
    '#maxlength' => 100,
    '#required' => TRUE,
    '#description' => t('The host to send PayPal requests to. Set this to
      www.paypal.com for live sites and www.sandbox.paypal.com for testing.'),
  );
  $form['lm_paypal_obey_test_ipns'] = array(
    '#type' => 'checkbox',
    '#title' => t('Process test IPNs'),
    '#default_value' => variable_get('lm_paypal_obey_test_ipns', LM_PAYPAL_OBEY_TEST_IPNS_DEFAULT),
    '#description' => t('Deal with test IPNs sent from a PayPal Sandbox as if they were real.'),
  );
  $form['lm_paypal_skip_validation'] = array(
    '#type' => 'value',
    '#value' => variable_get('lm_paypal_skip_validation', FALSE),
    '#title' => t('Disable validation of payments with paypal.com'),
    '#description' => t('This should NEVER be enabled, except for testing. Hidden from the UI for safety'),
  );
  $form['lm_paypal_advanced_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['lm_paypal_advanced_settings']['lm_paypal_ipns_max_age'] = array(
    '#type' => 'textfield',
    '#title' => t('Max lifetime of IPN'),
    '#default_value' => variable_get('lm_paypal_ipns_max_age', LM_PAYPAL_IPNS_MAX_AGE_DEFAULT),
    '#maxlength' => 10,
    '#required' => FALSE,
    '#validate' => array('lm_paypal_is_integer_between' => array(1)),
    '#description' => t('Maximum age (in hours) of an old IPN record before it is deleted. Type 0 if you want to keep them forever.'),
  );
  $form['lm_paypal_advanced_settings']['lm_paypal_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug mode'),
    '#default_value' => variable_get('lm_paypal_debug', LM_PAYPAL_DEBUG_DEFAULT),
    '#description' => t('Enable verbose output for debugging purposes.'),
  );
  return system_settings_form($form);
}

/**
 * Validates email address.
 */
function lm_paypal_settings_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['lm_paypal_business'])) {
    form_set_error('lm_paypal_business', t('Invalid email address!'));
  }
}

/**
 * View all saved IPNs.
 *
 * Mostly borrowed from the Watchdog module.
 */
function lm_paypal_ipns() {
  $ipns_per_page = 50;
  $filter = drupal_get_form('lm_paypal_ipns_filter');
  $output = drupal_render($filter);

  $header = array(
    array(
      'data' => t('Id'),
      'field' => 'id',
    ),
    array(
      'data' => t('Date'),
      'field' => 'timestamp',
      'sort' => 'desc',
    ),
    array(
      'data' => t('Txn Type'),
      'field' => 'txn_type',
    ),
    array(
      'data' => t('User'),
      // @todo Not 'custom' anymore.
      'field' => 'custom',
    ),
  );

  $select = db_select('lm_paypal_ipns', 'ipns')
    ->extend('TableSort')
    ->fields('ipns', array('id', 'timestamp', 'txn_type'))
    ->orderByHeader($header)
    ->extend('PagerDefault')
    ->limit($ipns_per_page);

  $type = $_SESSION['lm_paypal_ipns_filter'];
  if ($type != 'all') {
    if (strpos($type, '%') === FALSE) {
      $op = '=';
    }
    else {
      $op = 'like';
    }
    $select->condition('txn_type', $type, $op);
  }

  $result = $select->execute();
  $rows = array();
  foreach ($result as $ipn) {
    $custom = lm_paypal_unpack_ipn_custom($ipn);
    if (!empty($custom)) {
      $custom_data = $custom['uid'] . ($custom['other'] == '' ? '' : " ($custom[other])");
    }
    else {
      $custom_data = "This is a Test IPN";
    }
    $rows[] = array(
      'data' => array(
        l($ipn->id, "admin/config/lm_paypal/id/$ipn->id"),
        format_date($ipn->timestamp, 'short'),
        check_plain($ipn->txn_type),
        $custom_data,
      ),
    );
  }

  if (!$rows) {
    $rows[] = array(array(
        'data' => t('No ipns found.'),
        'colspan' => 3,
      ));
  }

  $output .= theme('table', array('header' => $header, 'rows' => $rows));
  $output .= theme('pager', array('tags' => NULL, 'element' => 0));

  return $output;
}

/**
 * Display a saved IPN.
 *
 * @param int $id
 *   The ID of the IPN to display. Required.
 *
 * @return string
 *   The details of the IPN as a string.
 */
function lm_paypal_id($id = '') {
  $id = check_plain($id);

  if ($id == '' || !is_numeric($id) || intval($id) != $id) {
    watchdog(LM_PAYPAL, 'Bad id passed: %id', array('%id' => $id), WATCHDOG_WARNING);
    return t('Huh?');
  }

  // Output the transaction as a table of fields/values (skip the empty ones).
  $output = '<h2>' . t('Transaction %id', array('%id' => $id)) . '</h2>';
  $header = array(t('field'), t('value'));

  $ipns = db_select('lm_paypal_ipns', 'ipns')
    ->fields('ipns')
    ->condition('id', $id)
    ->execute()
    ->fetch();
  $rows = array();
  foreach ($ipns as $key => $value) {
    if ($value == '' || $key == 'ipn') {
      continue;
    }
    if ($key == 'timestamp') {
      $value = format_date($value);
    }
    else {
      $value = check_plain($value);
    }
    $rows[] = array('data' => array($key, $value));
  }
  $output .= theme('table', array('header' => $header, 'rows' => $rows));
  return $output;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function lm_paypal_ipns_filter($form) {
  $names = array(
    'all' => t('all messages'),
    'web_accept' => t('donation/sent money (web_accept)'),
    'subscr_%' => t('all subscription IPNs'),
    'subscr_signup' => t('subscription signup'),
    'subscr_payment' => t('subscription payment'),
    'subscr_cancel' => t('subscription cancel'),
    'subscr_eot' => t('subscription eot'),
  );

  // @todo: Should we be using form state for this?
  if (empty($_SESSION['lm_paypal_ipns_filter'])) {
    $_SESSION['lm_paypal_ipns_filter'] = 'all';
  }

  $form['#submit'] = array('lm_paypal_ipns_submit');

  $form['filter'] = array(
    '#type' => 'select',
    '#title' => t('Filter IPN type'),
    '#options' => $names,
    '#default_value' => $_SESSION['lm_paypal_ipns_filter'],
  );
  $form['#action'] = url('admin/config/lm_paypal/base/ipns');

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );
  return $form;
}

/**
 * Process the form submission for lm_paypal_ipns.
 */
function lm_paypal_ipns_submit($form, &$form_state) {
  $_SESSION['lm_paypal_ipns_filter'] = $form_state['values']['filter'];
}
