<?php

/**
 * @file
 * Provides Rules integration.
 */

/**
 * Implements rules_event_info().
 */
function lm_paypal_event_info() {
  $items = array();
    $items['new_ipn'] = array(
      'label' => t('Received IPN from PayPal'),
      'group' => 'LM Paypal IPN',
      'variables' => array(
        'ipn' => array(
          'type' => 'ipn',
          'label' => t('IPN object'),
        ),
        'txn_type' => array(
          'type' => 'text',
          'label' => t('Transaction type'),
        ),
      ),
    );
  return $items;
}

/**
 * Implements hook_lm_paypal_ipn().
 *
 * @param $ipn
 *   The IPN.
 */
function lm_paypal_lm_paypal_ipn($ipn) {
  if (!$ipn->custom) {

    // Unpack custom.
    $custom = lm_paypal_unpack_ipn_custom($ipn);
    foreach ($custom as $k => $v) {
      $ipn->{$k} = $v;
    }
  }
  $txn_type = preg_replace('/[^a-z0-9_]*/', '', $ipn->txn_type);
  rules_invoke_event('new_ipn', $ipn, $txn_type);
}

/**
 * Implements hook_rules_data_info().
 */
function lm_paypal_rules_data_info() {
  $rules_data = array(
    'ipn' => array(
      'label' => t('Paypal IPN'),
      'wrap' => FALSE,
      'property info' => lm_paypal_ipn_property_info(),
      'token type' => 'ipn',
    ),
  );
  return $rules_data;
}

/**
 * @todo Needs documentation.
 */
function lm_paypal_ipn_property_info() {
  $schema = drupal_get_schema_unprocessed('lm_paypal', 'lm_paypal_ipns');
  $ipn_property_info = array();
  foreach ($schema['fields'] as $field => $values) {
    $ipn_property_info[$field] = array(
      'type' => lm_paypal_type_conversion($values['type']),
      'label' => $values['description'],
    );
  }
  $ipn_property_info['uid'] = array(
      'type' => 'integer',
      'label' => 'UID - This is passed in custom and may be overridden.',
    );
  return $ipn_property_info;
}

/**
 * Conversion of payment type.
 *
 * @param $type
 *   Needs description.
 */
function lm_paypal_type_conversion($type) {
  switch ($type) {
    case 'int':
    case 'serial':
      return 'integer';
      break;
    case 'varchar':
    case 'text':
    default:
      return 'text';
      break;
    case 'numeric':
      return 'decimal';
      break;
  }
}

/**
 * Implements hook_token_info().
 */
function lm_paypal_token_info() {
  $type = array(
    'name' => 'Paypal IPN',
    'description' => 'Tokens Related to Paypal IPNs',
    'needs-data' => 'ipn',
  );
  $schema = drupal_get_schema_unprocessed('lm_paypal', 'lm_paypal_ipns');
  $tokens = array();
  foreach ($schema['fields'] as $field => $values) {
    $tokens[$field] = array(
      'name' => $field,
      'description' => $values['description'],
    );
  }
  return array(
    'types' => array('ipn' => $type),
    'tokens' => array('ipn' => $tokens),
  );
}

/**
 * Implements hook_tokens().
 */
function lm_paypal_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  if ($type == 'ipn' && !empty($data['ipn'])) {
    $ipn = $data['ipn'];
    foreach ($tokens as $name => $original) {

      // Let other modules deal with this custom data.
      if ($name == "custom") {
        continue;
      }
      $replacements[$original] = (!empty($ipn->$name)) ? $ipn->$name : '';
    }
  }
  return $replacements;
}
