<?php

/**
 * @file
 * Receives and processes incoming IPNs from Paypal.
 */

/**
 * Page callback: Handles an incoming IPN.
 *
 * Path: lm_paypal/ipn
 *
 * PayPal sends one or more IPNs here for each transaction that takes place. The
 * IPN is present as a form submission which this routine unravels and saves for
 * processing.
 */
function lm_paypal_ipn_in() {
  watchdog(LM_PAYPAL, 'lm_paypal_ipn_in called', NULL);

  // Ignore the following fields but don't flag them as errors.
  static $ignore_fields = array(
    'receipt_id',
    'charset',
    'mc_gross1',
  );

  // Process the incoming form results to check for unexpected values.
  $schema = drupal_get_schema('lm_paypal_ipns');
  foreach ($_POST as $key => $value) {
    if ($value == '' || in_array($key, $ignore_fields)) {
      continue;
    }
    if (!isset($schema['fields'][$key])) {
      watchdog(LM_PAYPAL, 'IPN unknown field ignored: !key => !value', array('!key' => check_plain($key), '!value' => check_plain($value)), WATCHDOG_ERROR);
    }
  }
  $validated = _lm_paypal_validate_ipn($_POST);

  // Sanitize and remove risky fields from $_POST to make it suitable for
  // drupal_write_record. Include the entire POST data for debugging.
  $ipn = $_POST;
  $ipn['timestamp'] = time();
  $ipn['ipn'] = serialize($_POST);
  unset($ipn['processed']);

  // Add the IPN to our database (whether it validated or not).
  $wr = drupal_write_record('lm_paypal_ipns', $ipn);
  if ($wr == SAVED_NEW) {
    $last = $ipn['id'];
    watchdog(LM_PAYPAL, 'lm_paypal_ipn_in $last = ' . $ipn['id'], NULL);
    $link = l(t('view'), "admin/config/lm_paypal/id/$ipn[id]");
    if ($validated) {
      watchdog(LM_PAYPAL, 'IPN incoming %type', array('%type' => check_plain($ipn['txn_type'])), WATCHDOG_NOTICE, $link);
      lm_paypal_process_in((object) $ipn);
    }
    else {

      // @todo Actually return ->data
      $validate_result->data = '';
      watchdog(LM_PAYPAL, 'IPN incoming NOT VERIFIED %type got %ret', array('%type' => check_plain($_POST['txn_type']), '%ret' => check_plain($validate_result->data)), WATCHDOG_ERROR, $link);
    }
  }
  else {
    watchdog(LM_PAYPAL, 'IPN in failed to run sql: %sql', array('%sql' => check_plain($sql)), WATCHDOG_ERROR);
    // Return an HTTP error and hopefully PayPal will resend the IPN to me
    // later on and then I can try again! Maybe PayPal is very busy
    // or there is a network problem at the moment.
  }

  return 'IPN: Only PayPal will ever see this page - humans go away!';
}

/**
 * Process a newly arrived IPN message that has been verified and saved.
 *
 * @param int $ipn
 *   The ID of the saved IPN to be processed.
 */
function lm_paypal_process_in($ipn) {
  $business = lm_paypal_api_get_business();
  $link = l(t('view'), "admin/config/lm_paypal/id/$ipn->id");

  // Ignore IPN sent from a PayPal sandbox if this option is set.
  if ($ipn->test_ipn != '' && !variable_get('lm_paypal_obey_test_ipns', FALSE)) {
    watchdog(LM_PAYPAL, 'test_ipn received - ignoring', array(), WATCHDOG_WARNING, $link);
    return;
  }

  // Check that the received email matches the business email.
  if (strcasecmp(trim($ipn->receiver_email), trim($business)) != 0) {
    watchdog(
      LM_PAYPAL,
      'Incoming IPN received email does not match business email (received %received, business %business)',
      array('%received' => check_plain($ipn->receiver_email), '%business' => check_plain($business)),
      WATCHDOG_ERROR,
      $link);
    return;
  }

  // Every implementor of hook_lm_paypal_ipn() is free to take whatever
  // actions they want, based on the IPN, and return a boolean describing
  // whether they handled the IPN.  No attempt is made to prevent multiple
  // modules handling the same IPN.
  $results1 = module_invoke_all('lm_paypal_ipn', $ipn);

  // Additionally, IPNs are displatched to a txn_type-specific hook.
  $txn_type = preg_replace('/[^a-z0-9_]*/', '', $ipn->txn_type);
  $results2 = module_invoke_all('lm_paypal_ipn_' . $txn_type, $ipn);

  // Determine whether _some_ module handled the IPN, and warn if not.
  $processed = FALSE;
  foreach ($results1 as $res) {
    if ($res) {
      $processed = TRUE;
      break;
    }
  }
  if (!$processed) {
    foreach ($results2 as $res) {
      if ($res) {
        $processed = TRUE;
        break;
      }
    }
  }

  if (!$processed) {
    watchdog(LM_PAYPAL, 'No processor for this IPN, ignoring: %type', array('%type' => check_plain($ipn->txn_type)), WATCHDOG_WARNING, $link);
  }

  return;
}

/**
 * Implements hook_lm_paypal_ipn_TXNTYPE().
 * Processes a 'send_money' IPN message.
 *
 * @param int $ipn
 *   The ID of the IPN.
 */
function lm_paypal_lm_paypal_ipn_send_money($ipn) {
  if (lm_paypal_debug()) {
    watchdog(LM_PAYPAL, 'lm_paypal_lm_paypal_ipn_send_money (passing to web_accept)');
  }
  return lm_paypal_process_in_web_accept($ipn);
}

/**
 * Implementation of hook_lm_paypal_ipn_TXNTYPE.
 *
 * Processes a web_accept IPN message.
 *
 * @param $ipn
 *   The IPN.
 */
function lm_paypal_lm_paypal_ipn_web_accept($ipn) {
  _lm_paypal_process_in_web_accept($ipn);
  return TRUE;
}

/**
 * Process a newly arrived web_accept IPN message
 *
 * @param $ipn
 *   The IPN.
 */
function _lm_paypal_process_in_web_accept($ipn) {

  // Notify watchdog if debug mode is enabled.
  $debug = variable_get('lm_paypal_debug', LM_PAYPAL_DEBUG_DEFAULT);
  if ($debug) {
    watchdog(LM_PAYPAL, 'in_web_accept');
  }

  $link = l(t('view'), "admin/config/lm_paypal/id/$ipn->id");

  // Make sure the transaction hasn't already been processed.
  if (lm_paypal_already_processed($ipn->txn_id)) {
    watchdog(
      LM_PAYPAL,
      'This transaction has already been processed, ignored: %id',
      array('%id' => check_plain($ipn->txn_id)),
      WATCHDOG_WARNING,
      $link);
    return;
  }

  // Mark the transaction as processed.
  lm_paypal_mark_processed($ipn);

  // Make sure the payment is not pending.
  if ($ipn->payment_status == 'Pending') {
    watchdog(
      LM_PAYPAL,
      'Ignoring IPN with status: Pending. Check your PayPal account to see why it is pending. Note: pending_reason: %reason',
      array('%reason' => check_plain($ipn->pending_reason)),
      WATCHDOG_ERROR,
      $link);
    return;
  }

  $custom = lm_paypal_unpack_ipn_custom($ipn);
  $uid = (isset($custom['uid']))?$custom['uid']:'';
  $other = (isset($custom['other']))?$custom['other']:'';

  // Make sure UID is set.
  if ($uid == '') {
    if ($debug) {
      watchdog(
        LM_PAYPAL,
        'No UID, try to lookup payer_email',
        array(),
        WATCHDOG_WARNING,
        $link);
    }

    $uid = db_select('users', 'u')
      ->fields('u', array('uid'))
      ->where('LOWER(mail) = LOWER(:payer_email)', array(':payer_email' => $ipn->payer_email));
      ->execute()->fetchField();

    if (!$uid) {
      watchdog(
        LM_PAYPAL,
        'IPN web_accept no uid presuming UID 0, cannot find payer_email: %email',
        array('%email' => check_plain($ipn->payer_email)),
        WATCHDOG_WARNING,
        $link);
      $uid = 0;
    }
    else {
      watchdog(
        LM_PAYPAL,
        'IPN web_accept no UID, found payer_email %email for UID %uid',
        array('%email' => check_plain($ipn->payer_email), '%uid' => $uid),
        WATCHDOG_WARNING,
        $link);
    }
  }
  elseif (!is_numeric($uid) || intval($uid) != $uid || $uid < 0) {
    watchdog(
      LM_PAYPAL,
      'Invalid uid, ignoring IPN: %uid',
      array('%uid' => $uid),
      WATCHDOG_WARNING,
      $link);
    return;
  }

  // Assume that web_accept without a UID has come from an anonymous user.
  if ($uid != '') {

    // Check UID is valid.
    $user = db_select('users', 'u')
      ->fields('u')
      ->condition('uid', $uid)
      ->execute();
    if (!$user) {
      watchdog(
        LM_PAYPAL,
        'IPN web_accept unknown UID, presuming UID 0: %uid',
        array('%uid' => check_plain($uid)),
        WATCHDOG_ERROR,
        $link);
      $uid = 0;
    }
  }

  // Use the item_number to select the type of payment incoming.
  $item_number = $ipn->item_number;

  // Treat 'Send Money' menu item on PayPal the same as a donation
  // (item_number = 0)
  if ($ipn->txn_type == 'send_money') {
    if ($debug) {
      watchdog(LM_PAYPAL, "send_money - being converted to web_accept");
    }
    $item_number = 0;
  }
  elseif ($item_number == '') {
    if ($debug) {
      watchdog(LM_PAYPAL, "empty item_number - being converted to web_accept");
    }
    $item_number = 0;
  }
  elseif (!is_numeric($item_number) || intval($item_number) != $item_number || $item_number < 0) {
    watchdog(
      LM_PAYPAL,
      'Invalid item_number, ignoring IPN: %item_number',
      array('%item_number' => check_plain($item_number)),
      WATCHDOG_WARNING,
      $link);
    return;
  }

  return lm_paypal_web_accept_invoke($ipn, $link, $uid, $other, $item_number);
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function lm_paypal_web_accept_invoke($ipn, $link, $uid, $other, $item_number) {

  // HACK!
  // @todo Remove this and figure out how the web_accept path relates to top-
  // level IPNs.
  if (module_exists('lm_paypal_donations')) {
    include_once drupal_get_path('module', 'lm_paypal_donations') . '/lm_paypal_donations.module';
    if ($item_number == 0) {
      return _lm_paypal_process_in_donate($ipn, $link, $uid, $other, $item_number);
    }
  }

  watchdog(
    LM_PAYPAL,
    'No web_accept processor registered for this item_number: %item_number',
    array('%item_number' => check_plain($item_number)),
    WATCHDOG_WARNING,
    $link);
}

/**
 * Given the full $_POST data from an incoming IPN, check with PayPal that this
 * is a real IPN.  Without this check, IPNs may be spoofed.
 *
 * @param array $post
 *   The key-value pairs from an incoming IPN (presumably straight from $_POST)
 *
 * @return
 *   TRUE if PayPal successfully validated the request.  This will not happen if
 *   there was an error communicating with the service, the IPN did not
 *   originate from PayPal, or it was tampered with in any way.
 */
function _lm_paypal_validate_ipn($post) {

  // If lm_paypal_skip_validation is set then don't validate the incoming IPN
  // with PayPal. This should only be used for testing, for obvious reasons.
  if (variable_get('lm_paypal_skip_validation', FALSE)) {
    return TRUE;
  }

  // Build the IPN validation request from the $post fields.
  $req = 'cmd=_notify-validate';
  foreach ($post as $key => $value) {
    $req .= "&$key=" . urlencode(stripslashes($value));
  }

  // Validate the incoming IPN by sending it to PayPal for verification.
  $lm_paypal_host = variable_get('lm_paypal_host', LM_PAYPAL_HOST_DEFAULT);
  $validate_result = drupal_http_request(
    lm_paypal_api_get_host(TRUE),
    array(
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
      'method' => 'POST',
      'data' => $req,
    ),
  );
  return $validate_result->data == 'VERIFIED';
}
